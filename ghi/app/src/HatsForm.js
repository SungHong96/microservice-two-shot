import React, { useEffect, useState } from "react";

function HatForm(){
    const [locations, setLocations] = useState([]);
    const [color, setColor] = useState('');
    const [style, setStyle] = useState('');
    const [fabric, setFabric] = useState('');
    const [url, setUrl] = useState('');
    const [location, setLocation] = useState('');

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleUrlChange = (event) => {
        const value = event.target.value;
        setUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.color = color;
        data.style_name = style;
        data.fabric = fabric;
        data.url = url;
        data.location = location;
        console.log(data);

        const hatUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "applications/json",
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setColor('');
            setStyle('');
            setFabric('');
            setUrl('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const response = await fetch("http://localhost:8100/api/locations/");
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={style} onChange={handleStyleChange} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control"/>
                        <label htmlFor="style_name">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" id="fabric" name="fabric" className="form-control"/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={url} onChange={handleUrlChange} placeholder="Url" required type="text" id="url" name="url" className="form-control"/>
                        <label htmlFor="url">Picture Url</label>
                    </div>
                    <div className="mb-3">
                        <select value={location} onChange={handleLocationChange} required id="location" name = "location" className="form-select">
                        <option>Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option value={location.href} key={location.href}>
                                    {location.closet_name}
                                </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    );

}

export default HatForm;
