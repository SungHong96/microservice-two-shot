import React from "react";

function HatsList(props) {
    const deleteHat = (id) => {
        if (window.confirm("Are you sure? Like really sure? Seriously, there's no going back... I mean personally, we this this is a pretty beautiful hat. We would be extremely saddened to see it go. So... We ask again - are you sure?")) {
            fetch("http://localhost:8090/api/hats/" + id,
            { method: "DELETE" }).then(() => {
                window.location.reload();
            }).catch((err) => {
                console.log(err.message);
            })
        }
    }

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>ID</th>
                <th>Style Name</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Fabric</th>
                <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                        return (
                            <tr key={ hat.href }>
                                <td>{ hat.id }</td>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.color }</td>
                                <td><img src={ hat.url } alt="hat" width="75" height="75" /></td>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.location }</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => {deleteHat(hat.id) }}>Delete?</button>
                                </td>
                            </tr>
                        );
                    })}
            </tbody>
        </table>
        </>
    );

}

export default HatsList;
