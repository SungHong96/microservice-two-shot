import React from 'react'


function ShoeList(props) {
    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>ID</th>
                <th>Model Name</th>
                <th>Manufacturer</th>
                <th>Color</th>
                <th>Picture URL</th>
                <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {props.Shoe?.map(shoe => {
                        return (
                            <tr key={ shoe.href }>
                                <td>{ shoe.id }</td>
                                <td>{ shoe.modelname }</td>
                                <td>{ shoe.manufacturer }</td>
                                <td><img src={ shoe.url } alt="hat" width="75" height="75" /></td>
                                <td>{ shoe.color }</td>
                                <td>{ shoe.bin }</td>
                            </tr>
                        );
                    })}
            </tbody>
        </table>
        </>
    );

}

export default ShoeList;