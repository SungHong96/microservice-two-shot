import React, { useEffect, useState } from 'react';


function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState('');
    const [modelName, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [Color, setColor] = useState('');
    const [URL, setURL] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.bin = bin;
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.model_color = Color;
        data.model_picture_url = URL;

        console.log(data);

        const shoeURL = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "applications/json",
            },
        };

        const response = await fetch(shoeURL, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setModelName('');
            setManufacturer('');
            setColor('');
            setURL('');
            setBin('');
        }
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleURLChange = (event) => {
        const value = event.target.value;
        setURL(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
            }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add New Shoes</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">

              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={Color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="model_color">Color</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleURLChange} value={URL} placeholder="Model Picture Url" required type="text" name="model_picture_url" id="model_picture_url" className="form-control"/>
                <label htmlFor="model_picture_url">Picture URL</label>
              </div>

              <div className="mb-3">
                <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose Bin</option>
                  {bins.map(bin => {
                    return (
                        <option value={bin.href} key={bin.href}>
                            {bin.bin_number}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default ShoeForm