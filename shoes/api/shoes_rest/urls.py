from django.urls import path
from shoes_rest.views import list_shoes, show_shoes


urlpatterns = [
    path("shoes/", list_shoes, name="list_shoes"),
    path("bin/<int:bin_vo_id>/shoes/", list_shoes, name="api_list_shoes"),
    path("shoes/<int:id>/", show_shoes, name="show_shoes"),
]
