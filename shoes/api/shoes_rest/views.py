from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoes

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name", 
        "bin_number", 
        "bin_size", 
        "id",
    ]


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "model_color",
        "model_picture_url",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesDetailEncoder
        )
    else
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "ID Does not exist"},
                status=400,
            )
        newshoes = Shoes.objects.create(**content)
        return JsonResponse(
            newshoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET","DELETE"])
def show_shoes(request, id):
    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=id)
            return JsonResponse(
                shoes,
                encoder=ShoesDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        count , _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
