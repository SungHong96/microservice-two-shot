from django.contrib import admin
from shoes_rest.models import Shoes, BinVO

@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    pass

@admin.register(BinVO)
class ShoesVOAdmin(admin.ModelAdmin):
    pass
