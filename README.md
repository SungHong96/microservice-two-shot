# Wardrobify

Team:

* Sung Hong - Shoes
* Zachary Ceniceros - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

The Hats microservice will have a Hat model, which will have the properties of the hat's fabric, style name, color, a picture, and the location it is being held in the wardrobe. The wardrobe microservice will poll info from Hats.
