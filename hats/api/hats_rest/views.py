from django.http import JsonResponse
from common.json import ModelEncoder
from django.shortcuts import render
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
import json
# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "id",
    ]

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "style_name",
        "color",
        "url",
        "fabric",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_id = content['location']
            location = LocationVO.objects.get(import_href=location_id)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location ID"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )



@require_http_methods(["GET", "PUT", "DELETE", ])
def api_hat_detail(request, pk):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist!"},
                status=404,
            )
            return response

    elif request.method == "DELETE":
        try:
            hat = Hats.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist!"})
    else:
        try:
            content = json.loads(request.body)
            hat = Hats.objects.get(id=pk)

            props = [
                "fabric",
                "style_name",
                "color",
                "url",
                "location",
            ]

            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()

            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )

        except Hats.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )
