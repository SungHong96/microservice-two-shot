from django.contrib import admin
from .models import Hats, LocationVO
# Register your models here.

@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    list_display = (
        "style_name",
        "id",
    )

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = (
        "closet_name",
        "id",
    )
